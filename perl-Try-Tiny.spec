%global __provides_exclude_from %{?__provides_exclude_from:%__provides_exclude_from|}^%{_docdir}/
%define mod_name Try-Tiny

Name:          perl-Try-Tiny
Summary:       Minimal try/catch with proper preservation of $@
Version:       0.32
Release:       2
License:       MIT
URL:           https://metacpan.org/release/%{mod_name}
Source0:       https://cpan.metacpan.org/authors/id/E/ET/ETHER/%{mod_name}-%{version}.tar.gz

BuildArch:     noarch

BuildRequires: coreutils findutils perl-generators perl-interpreter
BuildRequires: perl(ExtUtils::MakeMaker) perl(Carp) perl(constant) perl(Exporter) >= 5.57 perl(strict)
BuildRequires: perl(Sub::Util) perl(warnings) perl(File::Spec) perl(Test::More) >= 0.96

Requires:      perl(Sub::Util)

%description
This module provides bare bones try/catch statements that are designed to minimize common mistakes with
eval blocks, and NOTHING else.

%prep
%autosetup -n %{mod_name}-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} -c $RPM_BUILD_ROOT

%check
make test

%files
%license LICENCE
%doc Changes CONTRIBUTING README t/
%{perl_vendorlib}/Try/
%{_mandir}/man3/Try::Tiny.3*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 0.32-2
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Dec 03 2024 xu_ping <707078654@qq.com> - 0.32-1
- Upgrade version to 0.32
  * skip given, when tests on perls >= 5.41.3 which removed these constructs

* Mon Oct 31 2022 hongjinghao <hongjinghao@huawei.com> - 0.31-2
- use %{mod_name} marco

* Tue Dec  7 2021 guozhaorui <guozhaorui1@huawei.com> - 0.31-1
- update version to 0.31

* Wed Oct 23 2019 Huiming Xie <xiehuiming@huawei.com> - 0.30-5
- init package

